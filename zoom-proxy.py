#!/usr/bin/env python3

import sys
import subprocess
import urllib.parse
import requests
import time
import datetime
from bs4 import BeautifulSoup

output_file = "/tmp/meeting_output.txt"

# Extract the meeting ID and meeting name from the Zoom link
def extract_meeting_info(link):
    parsed = urllib.parse.urlparse(link)
    params = urllib.parse.parse_qs(parsed.query)
    meeting_id = params.get('id', [''])[0]
    if meeting_id:
        return meeting_id, None
    conf_no = params.get('confno', [''])[0]
    return conf_no, None

    print(conf_no)

    # Scrape the meeting name from the Zoom join page
    if meeting_id or conf_no:
        response = requests.get(f'https://zoom.us/j/{meeting_id}')
        soup = BeautifulSoup(response.content, 'html.parser')
        meeting_name = soup.select_one('.zm-topic').text.strip()
        return meeting_id, meeting_name
    else:
        return None, None

# Get the Zoom meeting link passed as an argument
if len(sys.argv) < 2:
    with open(output_file, 'a') as file:
        file.write("No Zoom link provided.\n")
    sys.exit(1)

zoom_link = sys.argv[1]

# Extract the meeting ID and meeting name from the Zoom link
meeting_id, meeting_name = extract_meeting_info(zoom_link)


# Launch Zoom with the extracted meeting ID
if meeting_id:
    subp = subprocess.run(['zoom', zoom_link])
    with open(output_file, 'a') as file:
        now = datetime.datetime.now()
        file.write(f"Now: {now} Meeting ID: {meeting_id}  Name: {meeting_name} Link: {zoom_link}\n")
    # time.sleep(5)
    # subp = subprocess.run(['xdotool', 'search', '--onlyvisible', '-class', 'zoom'], capture_output = True, text = True)
    # print(subp.stdout)
    # subp = subprocess.run(['xdotool', 'windowactivate', subp.stdout], capture_output = True, text = True)
    # subp = subprocess.run(['xdotool', 'mousemove', '10', '10', 'click' , '3'], capture_output = True, text = True)
else:
    with open(output_file, 'a') as file:
        file.write("Invalid Zoom link.")
