```
$ xdg-mime query default x-scheme-handler/zoommtg
Zoom.desktop
```

Useful commands:

```
XDG_UTILS_DEBUG_LEVEL=2 xdg-mime default Zoom.desktop x-scheme-handler/zoomtg
XDG_UTILS_DEBUG_LEVEL=2 xdg-mime query default x-scheme-handler/zoomtg
XDG_UTILS_DEBUG_LEVEL=2 xdg-mime default zoom_handler.desktop x-scheme-handler/zoomtg
XDG_UTILS_DEBUG_LEVEL=2 xdg-mime query default x-scheme-handler/zoomtg

xdg-desktop-menu install zoom_handler.desktop --novendor

update-desktop-database ~/.local/share/applications/
```

